global exit
global string_length
global print_string
global print_err
global print_newline
global print_char
global print_int
global print_uint
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

%define DEC_BASE 10
%define INT_LENGTH 64
%define CHAR_LENGTH 1
%define NEW_LINE 0XA
%define MINUS 0x2D
%define SPACE 0x20
%define TAB 0X9
%define EOL 0
%define RAX_MAX_VALUE 0X20
%define READ_SYSCALL 0
%define WRITE_SYSCALL 1
%define STDIN 0
%define STDOUT 1
%define STDERR 2

section .text

; Принимает код возврата и завершает текущий процесс
exit:
    mov  rax, 60          
    syscall
; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
  .loop:
    cmp  byte [rdi + rax], EOL
    je   .end
    inc  rax
    jmp  .loop
  .end:
    mov  rdx, rax
    ret
; Принимает указатель на нуль-терминированную строку, выводит её в stderr
print_err:
    push rdi
    call string_length
    mov rsi, [rsp]
    mov rax, WRITE_SYSCALL     
    mov rdi, STDERR     
    syscall
    pop rdi
    ret
; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    mov  rsi, [rsp]
    mov  rax, WRITE_SYSCALL
    mov  rdi, STDOUT
    syscall
    pop rdi
    ret
; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, WRITE_SYSCALL
    mov rdi, STDOUT
    mov rdx, CHAR_LENGTH
    mov rsi, rsp
    syscall
    pop rdi
    ret
; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, NEW_LINE
    call print_char
    ret
; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push rbp
    mov rbp, rsp
    mov rax, rdi
    mov rdi, DEC_BASE
    dec rsp
    mov byte [rsp], EOL
    .loop:
        xor rdx, rdx
        div rdi
        add dl, '0'
        dec rsp
        mov [rsp], dl
        test rax, rax
        jnz .loop
    mov rdi, rsp
    mov rsp, rbp
    sub rsp, RAX_MAX_VALUE
    call print_string
    add rsp, RAX_MAX_VALUE
    pop rbp
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jns .non_neg
    neg rdi              
    push rdi  
    mov rdi, MINUS
    call print_char  
    pop rdi
    .non_neg:
	    call print_uint
    ret     
; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    .loop:
        mov dl, byte [rdi]
        mov cl, byte [rsi]
        cmp dl, cl
        jne .not_eq
        test cl, cl
        jz .eq
        inc rsi
        inc rdi
        jmp .loop
    .not_eq:
        ret
    .eq:
        mov rax, 1
        ret
; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    mov rax, READ_SYSCALL
    push rax
    mov rdi, STDIN
    mov rdx, CHAR_LENGTH
    mov rsi, rsp
    syscall
    cmp rax, 0
    jle return
    mov al, [rsi]
    return:
        add rsp, 8
    ret
; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    push rbx
    push rbp
    push r12
    xor rbx, rbx
    mov rbp, rsi
    mov r12, rdi
    .skip_spaces:
        call read_char
        cmp  al, SPACE
        je   .skip_spaces
        cmp  al, TAB
        je   .skip_spaces
        cmp  al, NEW_LINE
        je   .skip_spaces
    jmp .loop
    .overflow_check:
        cmp rbx, rbp    
        jge .fail   
    call read_char
    .loop: 
        cmp al, SPACE
        je .space
        cmp al, TAB
        je .space
        cmp al, NEW_LINE
        je .space
        cmp rbx, rbp    
        jge .fail   
        mov [r12 + rbx], al
        test al, al
        jz .success
        inc rbx
        jmp .overflow_check
    .space:
        test rbx, rbx
        jz .overflow_check
    .success:
        mov rax, r12
        mov rdx, rbx
        jmp .return
    .fail:
        xor rax, rax
        jmp .return
    .return:
        pop r12
        pop rbp
        pop rbx
        ret
; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rcx, rcx
    xor r8,  r8
    mov rsi,  DEC_BASE
.loop:
    mov cl,  [rdi+r8]
    cmp cl,  '0'
    jl  .return
    cmp cl,  '9'
    jg  .return
    sub cl,  '0'
    mul rsi
    add rax, rcx
    inc r8
    jmp .loop
.return:
    mov rdx, r8
    ret
; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte [rdi], MINUS
    jne parse_uint
		inc rdi
		call parse_uint
		test rdx, rdx
		jz .no_num
		inc rdx
		neg rax
		ret
	.no_num:
		xor rax, rax
		ret
; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    .loop:
        cmp rax, rdx
        jae .fail
        mov r8b, byte [rdi + rax]
        mov byte [rsi + rax], r8b
        test r8b, r8b
        jz .success
        inc rax
        jmp .loop
    .fail:
        xor rax, rax
    .success:
    ret
