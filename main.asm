%include "words.inc" 
%include "lib.inc"
%include "dict.inc"

%define BUF_SIZE 255
%define SHIFT 8

section .rodata
    enter_msg: db "Enter key:", 0
    overflow_msg: db "Overflow..", 0
    not_found_msg: db "Word not found", 0
    colon_separator: db ": ", 0

section .bss
    buffer: times BUF_SIZE resb 0

section .text
global _start

_start:
    mov rdi, enter_msg
    call print_string
.read:
    mov rsi, BUF_SIZE 
    mov rdi, buffer
    call read_word   
    push rdx                   
    test rax,rax
    jz .overflow
    mov rdi, rax
    mov rsi, third_word 
    call find_word
    test rax,rax
    jz .not_found
.print:
    mov rdi, rax
    add rax, SHIFT
    push rdi
    call print_string
    mov rdi, colon_separator
    call print_string
    pop rdi
    pop rdx
    inc rdi
    add rdi, rdx
    call print_string
    xor rdi,rdi
    call exit
.overflow:
    mov rdi, overflow_msg
    jmp .print_err
.not_found:
    mov rdi, not_found_msg
.print_err:
    call print_err
    pop rdx
    mov rdi, 1
    call exit
