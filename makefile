ASM=nasm
ASMFLAGS=-felf64
LD=ld
PY=python

prog: main.o lib.o dict.o
	$(LD) -o $@ $^
	
%.o: %.asm %.inc
	$(ASM) $(ASMFLAGS) -o $@ $<

test:
	@$(PY) test.py

clean:
	$(RM) *.o
	$(RM) program
	
.PNONY: clean
