import subprocess
import unittest

def run_program(input_text):
    try:
        result = subprocess.run(
            ['./main'], 
            input=input_text,
            text=True,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            check=True
        )
        return result.stdout
    except subprocess.CalledProcessError as e:
        return subprocess.CalledProcessError.stderr

class TestProgram(unittest.TestCase):
    def test_third_word(self):
        input_text = "third_word"
        expected_output = "third_word: third word explanation"
        output = run_program(input_text)
        self.assertEqual(output.strip(), expected_output.strip())

    def test_second_word(self):
        input_text = "second_word"
        expected_output = "second_word: second word explanation"
        output = run_program(input_text)
        self.assertEqual(output.strip(), expected_output.strip())

    def test_first_word(self):
        input_text = "first_word"
        expected_output = "first_word: first word explanation"
        output = run_program(input_text)
        self.assertEqual(output.strip(), expected_output.strip())

    def test_not_found(self):
        input_text = "fourth_word"
        expected_output = "Word not found"
        output = run_program(input_text)
        self.assertEqual(output.strip(), expected_output.strip())
    
    def test_255_string(self):
        input_text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Fermentum iaculis eu non diam phasellus vestibulum lorem. Sit amet luctus venenatis lectus magna fringilla urna porttitor rhoncus.."
        expected_output = "Word not found"
        output = run_program(input_text)
        self.assertEqual(output.strip(), expected_output.strip())
        
    def test_empty_string(self):
        input_text = ""
        expected_output = "Word not found"
        output = run_program(input_text)
        self.assertEqual(output.strip(), expected_output.strip())

    def test_overflow(self):
        input_text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Fermentum iaculis eu non diam phasellus vestibulum lorem. Sit amet luctus venenatis lectus magna fringilla urna porttitor rhoncus. Ut venenatis tellus in metus. Maecenas accumsan lacus vel facilisis. Gravida rutrum quisque non tellus orci ac auctor augue mauris. Quis risus sed vulputate odio ut enim blandit volutpat maecenas. Sed elementum tempus egestas sed. In aliquam sem fringilla ut morbi. Quam elementum pulvinar etiam non quam. Sagittis id consectetur purus ut faucibus pulvinar elementum integer. Ipsum faucibus vitae aliquet nec ullamcorper sit. Fringilla phasellus faucibus scelerisque eleifend donec pretium. Nec dui nunc mattis enim ut tellus elementum. Donec et odio pellentesque diam volutpat commodo sed egestas egestas. Ultricies integer quis auctor elit sed vulputate mi sit amet. Vitae auctor eu augue ut lectus. Donec ac odio tempor orci dapibus ultrices in iaculis nunc."
        expected_output = "Overflow.."
        output = run_program(input_text)
        self.assertEqual(output.strip(), expected_output.strip())

if __name__ == "__main__":
    unittest.main()
