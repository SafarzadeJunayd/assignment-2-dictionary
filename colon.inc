%define cur_el 0
%macro colon 2
    %ifid %2
        %2: dq cur_el
        %define cur_el %2
    %else
        %error "the second argument isn't an id"
    %endif
    %ifstr %1
        db %1, 0
    %else
        %error "the first argument isn't a string"
    %endif
%endmacro
