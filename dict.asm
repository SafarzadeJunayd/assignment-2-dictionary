%include "lib.inc"
%define SHIFT 8 
global find_word

section .text

find_word:
    push r12
    push r13
    mov r12, rdi               
    mov r13, rsi   
    .loop:
        test r13, r13 
        jz .fail
        test r12, r12
        jz .fail
        lea rsi, [r13 + SHIFT]
        call string_equals
        test rax, rax
        jnz .success
        mov rsi, [rsi] 
        jmp .loop
    .fail: 
        xor rax, rax
        jmp .end
    .success: 
        mov rax, rsi
    .end:
        pop r13
        pop r12
        ret
    